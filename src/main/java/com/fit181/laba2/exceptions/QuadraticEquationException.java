package com.fit181.laba2.exceptions;

public class QuadraticEquationException extends RuntimeException {
    public QuadraticEquationException(String message) {
        super(message);
    }
}
