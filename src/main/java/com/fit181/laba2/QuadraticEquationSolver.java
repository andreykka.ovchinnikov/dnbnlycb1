package com.fit181.laba2;

import com.fit181.laba2.exceptions.QuadraticEquationException;

public class QuadraticEquationSolver {
    private QuadraticEquationSolver() {
    }

    public static double[] solve(double a, double b, double c) {
        double epsilon = 0.00001;
        if (Math.abs(a) < epsilon) {
            throw new QuadraticEquationException("Coefficient a is 0!");
        }

        double determinant = Math.pow(b, 2) - 4 * a * c;

        if (determinant < 0) {
            throw new QuadraticEquationException("Discriminant is less than zero!");
        } else if (Math.abs(determinant) < epsilon) {
            return new double[]{-b / (2 * a)};
        }

        double x1 = (-b + Math.sqrt(determinant)) / (2 * a);
        double x2 = (-b - Math.sqrt(determinant)) / (2 * a);

        return new double[]{x1, x2};
    }
}
