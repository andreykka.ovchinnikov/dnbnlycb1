package com.fit181.laba2;

import com.fit181.laba2.exceptions.QuadraticEquationException;
import org.apache.commons.collections4.CollectionUtils;
import org.junit.jupiter.api.Test;

import java.util.stream.Collectors;
import java.util.stream.DoubleStream;

import static org.junit.jupiter.api.Assertions.*;

class QuadraticEquationSolverTest {

    @Test
    void solve_withOneRoot() {
        double[] res = QuadraticEquationSolver.solve(1, -2, 1);
        assertTrue(CollectionUtils.isEqualCollection(
            DoubleStream.of(1.0d).boxed().collect(Collectors.toList()),
            DoubleStream.of(res).boxed().collect(Collectors.toList())
            )
        );
    }

    @Test
    void solve_withTwoRoots() {
        double[] res = QuadraticEquationSolver.solve(1, 0, -1);
        assertTrue(CollectionUtils.isEqualCollection(
            DoubleStream.of(-1.0d, 1.0d).boxed().collect(Collectors.toList()),
            DoubleStream.of(res).boxed().collect(Collectors.toList())
            )
        );
    }

    @Test
    void solve_withNoRoots() {
        assertThrows(QuadraticEquationException.class, () -> {
            QuadraticEquationSolver.solve(1, 0, 1);
        }, "Discriminant is less than zero!");
    }

    @Test
    void solve_coefIsZero() {
        assertThrows(QuadraticEquationException.class, () -> {
            QuadraticEquationSolver.solve(0, 0, 1);
        }, "Coefficient a is 0!");
    }
}